#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <string>
#include <iostream>
#include <sstream>

#define SERVER_PORT 6969
#define QUEUE_SIZE 5
#define MESSAGE_SIZE 100000

using namespace std;

//struktura zawierajÄca dane, ktĂłre zostanÄ przekazane do wÄtku
struct thread_data_t
{
    int socketId;
};

//funkcja opisujÄcÄ zachowanie wÄtku - musi przyjmowaÄ argument typu (void *) i zwracaÄ (void *)
void *ThreadBehavior(void *t_data)
{
    int sendSuccess = 0;
    int recvSuccess = 0;
    string controlMessage = "RDY";
    string enemyReadyMessage = "GO";
    pthread_detach(pthread_self());
    struct thread_data_t *th_data = (struct thread_data_t*)t_data;
    char message[MESSAGE_SIZE];
    cout << "Connected: " << th_data->socketId << endl;
    recvSuccess = recv(th_data->socketId, &message, 5, 0);
    cout << recvSuccess << message << endl;
    if (recvSuccess < 0 || string(message).rfind(controlMessage, 0) != 0) {
        cout << "Blad przy wykryciu poprawnego klienta" << endl;
        pthread_exit(NULL);
    }
    // send client id
    sleep(1);
    string clientId = to_string(th_data->socketId);
    clientId += "\n";
    sendSuccess = send(th_data->socketId, clientId.c_str(), clientId.size(), 0);
    if (sendSuccess < 0) {
        cout << "Blad przy wysylaniu client id";
        pthread_exit(NULL);
    }

    int enemyId = 0;
    // recive enemy id
    recvSuccess = recv(th_data->socketId, &message, MESSAGE_SIZE, 0);
    enemyId = atoi(message);
    
    cout << "Enemy id: " << message << " " << enemyId << endl;
    if (recvSuccess < 0) {
        cout << "Blad przy odebraniu enemy id";
        pthread_exit(NULL);
    }

    string challengeMessage = "EI";
    challengeMessage += clientId;

    sendSuccess = send(enemyId, challengeMessage.c_str(), challengeMessage.size(), 0);
    if (sendSuccess < 0) {
        pthread_exit(NULL);
    }
    
    cout << message << endl;
    while (true) {
        recvSuccess = recv(th_data->socketId, &message, MESSAGE_SIZE, 0);
        // cout << "client: " << th_data->socketId << " Enemy: " << enemyId << " " << message << endl;
        cout << "Message: " << message << endl;
        if (recvSuccess < 0) {
            pthread_exit(NULL);
        }
        sendSuccess = send(enemyId, &message, recvSuccess, 0);
        if (sendSuccess < 0) {
            pthread_exit(NULL);
        }
    }    

    pthread_exit(NULL);
}

//funkcja obsĹugujÄca poĹÄczenie z nowym klientem
void handleConnection(int connection_socket_descriptor) {
    //wynik funkcji tworzÄcej wÄtek
    int create_result = 0;

    //uchwyt na wÄtek
    pthread_t thread1;

    struct thread_data_t *t_data = new thread_data_t();
    t_data->socketId = connection_socket_descriptor;

    //dane, ktĂłre zostanÄ przekazane do wÄtku
    //TODO dynamiczne utworzenie instancji struktury thread_data_t o nazwie t_data (+ w odpowiednim miejscu zwolnienie pamiÄci)
    //TODO wypeĹnienie pĂłl struktury

    create_result = pthread_create(&thread1, NULL, ThreadBehavior, (void *)t_data);
    if (create_result){
       printf("BĹÄd przy prĂłbie utworzenia wÄtku, kod bĹÄdu: %d\n", create_result);
       exit(-1);
    }

    //TODO (przy zadaniu 1) odbieranie -> wyĹwietlanie albo klawiatura -> wysyĹanie
}

int main(int argc, char* argv[])
{
   int server_socket_descriptor;
   int connection_socket_descriptor;
   int bind_result;
   int listen_result;
   char reuse_addr_val = 1;
   struct sockaddr_in server_address;

   //inicjalizacja gniazda serwera
   
   memset(&server_address, 0, sizeof(struct sockaddr));
   server_address.sin_family = AF_INET;
   server_address.sin_addr.s_addr = htonl(INADDR_ANY);
   server_address.sin_port = htons(SERVER_PORT);

   server_socket_descriptor = socket(AF_INET, SOCK_STREAM, 0);
   if (server_socket_descriptor < 0)
   {
       fprintf(stderr, "%s: BĹÄd przy prĂłbie utworzenia gniazda..\n", argv[0]);
       exit(1);
   }
   setsockopt(server_socket_descriptor, SOL_SOCKET, SO_REUSEADDR, (char*)&reuse_addr_val, sizeof(reuse_addr_val));

   bind_result = bind(server_socket_descriptor, (struct sockaddr*)&server_address, sizeof(struct sockaddr));
   if (bind_result < 0)
   {
       fprintf(stderr, "%s: BĹÄd przy prĂłbie dowiÄzania adresu IP i numeru portu do gniazda.\n", argv[0]);
       exit(1);
   }

   listen_result = listen(server_socket_descriptor, QUEUE_SIZE);
   if (listen_result < 0) {
       fprintf(stderr, "%s: BĹÄd przy prĂłbie ustawienia wielkoĹci kolejki.\n", argv[0]);
       exit(1);
   }

   while(1)
   {
       connection_socket_descriptor = accept(server_socket_descriptor, NULL, NULL);
       if (connection_socket_descriptor < 0)
       {
           fprintf(stderr, "%s: BĹÄd przy prĂłbie utworzenia gniazda dla poĹÄczenia.\n", argv[0]);
           exit(1);
       }

       handleConnection(connection_socket_descriptor);
   }

   close(server_socket_descriptor);
   return(0);
}
